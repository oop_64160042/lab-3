import java.util.Scanner;
public class Problem4 {
    public static void main(String[] args) {
        int Nums;
        int Sum = 0;
        int Count = 0;
        double Average = 0;
        Scanner sc = new Scanner(System.in);
        
        do{ //เข้าเงื่อนไข
            System.out.print("Please input number: ");
            Nums = sc.nextInt();
            // หาSum
            Sum = Sum + Nums;
            // หาAverage
            Count++;
            Average = Sum/Count;
            System.out.println("Sum: " + Sum + ", Avg: " + Average);
        } while (Nums != 0); //ไม่เข้าเงื่อนไข
        System.out.println("Bye");

        sc.close();
    }
}
