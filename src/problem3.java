import java.util.Scanner;

public class problem3 {
    public static void main(String[] args) {
        int FirstNum;
        int SecondNum;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number: ");
        FirstNum = sc.nextInt(); //input 1

        System.out.print("Please input second number: ");
        SecondNum = sc.nextInt(); //input 2

        System.out.println(FirstNum + " " + SecondNum);

        if(FirstNum > SecondNum){
            System.out.println("Error");
        } else {
            //loop
            for(int i = FirstNum; i < SecondNum+1 ; i++ ) {
                System.out.print(i + " ");
            }
        }
        sc.close();
        
    }
}
