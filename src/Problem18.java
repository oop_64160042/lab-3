import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        String type ;
        int nums ;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please select star type [1-4,5 is Exit]: ");
        type = sc.next(); //input 1

        //type 1
        if (type.trim().equalsIgnoreCase("1")) {
            System.out.print("Please input number: ");
            nums = sc.nextInt(); //input 2
            for (int i = 0 ; i < nums ; i++){
                for (int j = 0 ; j < i+1 ; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }

        //type 2
        } else if (type.trim().equalsIgnoreCase("2")) {
            System.out.print("Please input number: ");
            nums = sc.nextInt(); //input 2
            for (int i = nums ; i > 0 ; i--){
                for (int j = 0 ; j < i ; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }

        //type 3   
        } else if (type.trim().equalsIgnoreCase("3")) {
            System.out.print("Please input number: ");
            nums = sc.nextInt(); //input 2
            for (int i = 0 ; i < nums ; i++){
                for (int j = 0 ; j < i ; j++) {
                    System.out.print(" ");
                }
                for(int j = 0 ; j < nums-i ; j++){
                    System.out.print("*");
                }
            System.out.println();
            }

        //type 4
        } else if (type.trim().equalsIgnoreCase("4")) {
            System.out.print("Please input number: ");
            nums = sc.nextInt(); //input 2
            for (int i = nums-1 ; i >= 0 ; i--){
                for (int j = 0 ; j < nums+1 ; j++) {
                    if(j<=i) {
                        System.out.print(" ");
                    } else {
                        System.out.print("*");
                    }
                }
            System.out.println();
            }

        //type 5 (exit)
        } else if (type.trim().equalsIgnoreCase("5")){
            System.out.print("Bye bye!!!");

        //other
        } else {
            System.out.print("Error: Please input number between 1-5");
        }
        
    }
}
